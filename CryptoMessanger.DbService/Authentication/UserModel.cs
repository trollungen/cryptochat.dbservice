﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoMessanger.DbService
{
    public class UserModel
    {
        public Guid Id { get; set; }
        public string Username { get; set; }
    }
}

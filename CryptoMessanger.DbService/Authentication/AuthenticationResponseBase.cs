﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoMessanger.DbService
{
    public class AuthenticationResponseBase
    {
        public AuthenticationResponseBase()
        {
            this.Exception = null;
        }

        public Exception Exception { get; set; }
    }
}

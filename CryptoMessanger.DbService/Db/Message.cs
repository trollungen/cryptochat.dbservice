﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoMessanger.DbService.Db
{
    [Table("Message")]
    public class Message
    {
        [Key]
        public int ID { get; set; }

        public Guid UserId { get; set; }
        public string MessageText { get; set; }
        public string SendersUsername { get; set; }
        public bool IsEncrypted { get; set; }
        public string SentDateTime { get; set; }

        public virtual User User { get; set; }
    }
}

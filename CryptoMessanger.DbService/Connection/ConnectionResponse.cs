﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoMessanger.DbService
{
    public class ConnectionResponse : ConnectionResponseBase
    {
        public string ConnectionError { get; set; }
        public bool ConnectionStatus { get; set; }
        public Guid UserID { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CryptoMessanger.DbService.Db;

namespace CryptoMessanger.DbService
{
    public class AuthenticationService : IAuthenticationService
    {
        
        public AuthenticationResponse TryToRegister(string username, string password)
        {
            AuthenticationResponse response = new AuthenticationResponse();
            try
            {
                using (var db = new CryptoContext())
                {
                    var user = db.Users.FirstOrDefault(i => i.UserName == username);
                    if (user == null)
                    {

                        var crypto = new SimpleCrypto.PBKDF2();
                        var hashpassword = crypto.Compute(password);
                        var newuser = db.Users.Create();

                        newuser.Id = Guid.NewGuid();
                        newuser.UserName = username;
                        newuser.PasswordHash = hashpassword;
                        newuser.PasswordSalt = crypto.Salt;
                        newuser.Key = GeneratePublicKey();

                        db.Users.Add(newuser);
                        db.SaveChanges();

                        response.IsValid = true;
                    }
                    else
                    {
                        response.IsValid = false;
                        response.AuthenticationError = "user already exist";
                    }
                    
                }
            }
            catch (Exception ex)
            {
                response.Exception = ex;
                response.AuthenticationError = "Something went wrong when try to register new user";
                throw;
            }
            return response;
        }

        public AuthenticationResponse TryToLogin(string username, string password)
        {
            AuthenticationResponse response = new AuthenticationResponse();
            try
            {
                using (var db = new CryptoContext())
                {
                    var crypto = new SimpleCrypto.PBKDF2();
                    var user = db.Users.FirstOrDefault(i => i.UserName == username);
                    if (user != null)
                    {
                        if (user.PasswordHash == crypto.Compute(password, user.PasswordSalt))
                        {
                            response.IsValid = true;
                        }
                        else
                        {
                            response.IsValid = false;
                            response.AuthenticationError = "Username or password is invalid";
                        }
                    }
                    else
                    {
                        response.AuthenticationError = "User do not exist";
                        response.IsValid = false;
                    }
                }
            }
            catch (Exception ex)
            {
                response.Exception = ex;
                response.AuthenticationError = "Something went wrong when trying to login";
            }
            return response;
        }


        private string GeneratePublicKey()
        {
            var random = new Random();
            var chars = "1346798520";
            var result = new string(Enumerable.Repeat(chars, 32).Select(s => s[random.Next(s.Length)]).ToArray());
            return result;
        }
    }
}

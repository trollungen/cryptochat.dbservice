﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoMessanger.DbService
{
    public class AuthenticationResponse : AuthenticationResponseBase
    {
        public bool IsValid { get; set; }
        public string AuthenticationError { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoMessanger.DbService
{
    public class MessangerModel : MessangerBaseResponse
    {
        public int Id { get; set; }
        public string Message { get; set; }
        public int RecieverId { get; set; }
        public string RecieverUsername { get; set; }
        public string SenderUsername { get; set; }
        public string SentDateTime { get; set; }

        public bool IsEncrypted { get; set; }

    }
}

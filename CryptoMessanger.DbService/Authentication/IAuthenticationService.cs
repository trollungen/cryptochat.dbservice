﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoMessanger.DbService
{
    public interface IAuthenticationService
    {
        AuthenticationResponse TryToRegister(string username, string password);
        AuthenticationResponse TryToLogin(string username, string password);
    }
}

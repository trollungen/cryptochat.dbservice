﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoMessanger.DbService.Db
{
    [Table("User")]
    public class User
    {
        [Key]
        public Guid Id { get; set; }
        public string UserName { get; set; }

        public string PasswordHash { get; set; }
        public string PasswordSalt { get; set; }

        public bool IsConnected { get; set; }
        public string ConnectionId { get; set; }

        public string Key { get; set; }

        public virtual ICollection<Message> Messages { get; set; }
    }
}

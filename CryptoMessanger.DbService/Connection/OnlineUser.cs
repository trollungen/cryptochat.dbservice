﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoMessanger.DbService
{
    public class OnlineUser
    {
        public int OnlineUserId { get; set; }
        public string OnlineUserName { get; set; }
        public string ConnectionId { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoMessanger.DbService
{
    public interface IMessageService
    {
        List<UserModel> AllUsersExceptCurrent(string username);
        MessageResponse SendMessage(MessangerModel message);
        List<MessangerModel> GetListOfMessage(string username);
        void DeleteMessage(string message);
        string GetUserPublicKey(string username);
    }
}

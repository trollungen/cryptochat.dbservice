﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CryptoMessanger.DbService.Db;

namespace CryptoMessanger.DbService
{
    public class MessangerService : IMessageService
    {
        public List<UserModel> AllUsersExceptCurrent(string username)
        {
            try
            {
                using (var db = new CryptoContext())
                {
                    List<UserModel> allusers = new List<UserModel>();
                    foreach (var user in db.Users.Where(i => i.UserName != username))
                    {
                        var list = new UserModel()
                        {
                            Id = user.Id,
                            Username = user.UserName,
                        };
                        allusers.Add(list);
                    }
                    return allusers;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public MessageResponse SendMessage(MessangerModel message)
        {
            MessageResponse response = new MessageResponse();
            try
            {
                using (var db = new CryptoContext())
                {
                    var userToSendTo = db.Users.FirstOrDefault(i => i.UserName == message.RecieverUsername);
                    var msg = new Message()
                    {
                        UserId = userToSendTo.Id,
                        MessageText = message.Message,
                        SendersUsername = message.SenderUsername,
                        IsEncrypted = message.IsEncrypted,
                        SentDateTime = DateTime.Now.ToString(),
                    };

                    userToSendTo.Messages.Add(msg);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                response.Exception = ex;
                response.MessageError = "Something went wrong trying to send message";
            }
            return response;
        }

        public List<MessangerModel> GetListOfMessage(string username)
        {
            List<MessangerModel> listOfmessages = new List<MessangerModel>();
            try
            {
                using (var db = new CryptoContext())
                {
                    var user = db.Users.FirstOrDefault(i => i.UserName == username);
                    var msg = db.Messages.Where(i => i.UserId == user.Id).ToList();

                    foreach (var mess in msg)
                    {
                        var messages = new MessangerModel()
                        {
                            SenderUsername = mess.SendersUsername,
                            Message = mess.MessageText,
                            IsEncrypted = mess.IsEncrypted,
                            SentDateTime = mess.SentDateTime,

                        };
                        listOfmessages.Add(messages);
                    }
                }
            }
            catch (Exception ex)
            {
                var error = new MessangerModel() { Exception = ex };
                listOfmessages.Add(error);
            }
            return listOfmessages;
        }

        public void DeleteMessage(string message)
        {
            try
            {
                using (var db = new CryptoContext())
                {
                    var msg = db.Messages.FirstOrDefault(i => i.MessageText == message);
                    db.Messages.Remove(msg);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetUserPublicKey(string username)
        {
            using (var db = new CryptoContext())
            {
                var user = db.Users.FirstOrDefault(i => i.UserName == username);

                return user.Key;
            }
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoMessanger.DbService
{
    public class MessageResponse : MessangerBaseResponse
    {
        public string MessageError { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CryptoMessanger.DbService
{
    public interface IConnectionService
    {
        ConnectionResponse UpdateConnectionOnUser(string username, string connectionid, bool connectionstatus);
        List<OnlineUser> GetConnectedUserList();
    }
}

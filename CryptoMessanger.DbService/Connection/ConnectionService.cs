﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CryptoMessanger.DbService.Db;

namespace CryptoMessanger.DbService
{
    public class ConnectionService : IConnectionService
    {
        public ConnectionResponse UpdateConnectionOnUser(string username, string connectionid, bool connectionstatus)
        {
            ConnectionResponse response = new ConnectionResponse();
            try
            {
                using (var db = new CryptoContext())
                {
                    var updateuser = db.Users.FirstOrDefault(i => i.UserName == username);

                    if (updateuser != null)
                    {
                        updateuser.IsConnected = connectionstatus;
                        updateuser.ConnectionId = connectionid;
                        db.SaveChanges();

                        response.UserID = updateuser.Id;
                        response.ConnectionStatus = updateuser.IsConnected;
    
                    }
                    else
                    {
                        response.ConnectionError = "Did´nt find user";
                    }
                }
            }
            catch (Exception ex)
            {
                response.Exception = ex;
                response.ConnectionStatus = false;
                throw;
            }
            return response;
        }

        public List<OnlineUser> GetConnectedUserList()
        {
            List<OnlineUser> onlinelist = new List<OnlineUser>();
            try
            {
                using (var db = new CryptoContext())
                {
                    var connected = db.Users.Where(i => i.IsConnected == true).ToList();

                    foreach (var user in connected)
                    {
                        var onlineuser = new OnlineUser() { OnlineUserName = user.UserName, ConnectionId = user.ConnectionId };
                        onlinelist.Add(onlineuser);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return onlinelist;

        }
    }
}
